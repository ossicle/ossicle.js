/* [DOC]
## Ossicle.js `0.0.1`

> 🚧 this is a work in progress

**TODO**:
- redux?
- models?

[DOC] */


/* [DOC]
Ref: https://vanillajstoolkit.com/helpers/sanitizehtml/
🤔 🚧
[DOC] */
function sanitizeHTML (str) {
  // foo
}

/* [DOC]
[DOC] */
let Style = {
  sheets: {},
  css: (cssString, ...values) => {
    const sheet = new CSSStyleSheet()
    sheet.replaceSync(
      cssString.map((stringRow, index) =>
        stringRow + (values[index]!==undefined ? values[index] : "")
      ).join("")
    )
    return sheet
  }
}

/* [DOC]
[DOC] */
async function styleSheetPromise(cssPath, variableName, apply) {
  try {
    const response = await fetch(cssPath, {
      mode: 'no-cors',
      credentials: 'same-origin' // include, *same-origin, omit
    })
    const cssRaw = await response.text()
    const sheet = new CSSStyleSheet()
    sheet.replaceSync(cssRaw)
    // save the css result to a global variable to share styles
    // with web components
    Style.sheets[variableName] = sheet
    sheet.applyToAllDocument = apply
    return sheet
  }
  catch (error) {
    return error
  }
}

/* [DOC]
[DOC] */
let Initialize = {
  css: (sheets) => Promise.all(
    sheets.map(row => styleSheetPromise(row.sheet, row.name, row.apply))
  ).then(styleSheets => {
    // apply styles to the main document (`body, html, ...`)
    document.adoptedStyleSheets = styleSheets.filter(sheets => sheets.applyToAllDocument==true)
  })
}

/* [DOC]
[DOC] */
let Template = {
  html: (templateString, ...values) => {
    //console.log(templateString)
    const template = document.createElement('template')
    template.innerHTML = templateString.map((stringRow, index) =>
      stringRow + (values[index]!==undefined ? values[index] : "")
      //stringRow + (values[index]!==undefined ? sanitizeHTML(values[index]) : "")
    ).join("")
    template.clone = _ => template.content.cloneNode(true)
    return template
  }
}

/* [DOC]
🚧 🤔
[DOC] */
let Fragment = {
  html: (htmlString, ...values) => {
    return htmlString.map((stringRow, index) =>
      stringRow + (values[index]!==undefined ? values[index] : "")
    ).join("")
  }
}

/* [DOC]
[DOC] */
class Ossicle extends HTMLElement {

  static template() { return null }
  static sheets() { return [] }

  constructor() {
    super()
    this.attachShadow({mode: 'open'})
  }

  appendChild(clonedTemplate) {
    this.shadowRoot.appendChild(clonedTemplate)
  }

  adoptedStyleSheets(sheets) { // Array
    this.shadowRoot.adoptedStyleSheets = sheets
  }

  setup(clonedTemplate, sheets) {
    if(clonedTemplate && sheets) {
      this.appendChild(clonedTemplate)
      this.adoptedStyleSheets(sheets)
    } else {
      this.appendChild(this.constructor.template().clone())
      this.adoptedStyleSheets(this.constructor.sheets())
    }
  }

  $(selector) {
    return this.shadowRoot.querySelector(selector)
  }

  $all(selector) {
    return this.shadowRoot.querySelectorAll(selector)
  }

  $class(name) {
    return this.$(`.${name}`)
  }

  $classes(name) {
    return this.$all(`.${name}`)
  }

  $name(name) {
    return this.$(`[name="${name}"]`)
  }

  $id(id) {
    return this.shadowRoot.getElementById(id)
  }

  sendEvent(messageType, content) {
    this.dispatchEvent(
      new CustomEvent(messageType, {
        bubbles: true, composed: true, detail: content
      })
    )
  }

}

/* [DOC]
[DOC] */
class Router {

  static start(callBack) {
    let parseLocation = _ => {
      return location.hash.slice(1).toLowerCase() || '/'
    }
    // first time
    let router_event = {
      type: "load",
      location: parseLocation()
    }
    // send the location of the first load to the listener
    // (most of the time, it's the main component of the web app)
    callBack(router_event)

    let sendRouterEvent = (event) => {
      let router_event = {
        type: event.type,
        location: parseLocation()
      }
      callBack(router_event)
    }

    window.addEventListener('hashchange', sendRouterEvent)
    //window.addEventListener('load', sendRouterEvent)
  }

}

export {Style, Initialize, Template, Fragment, Ossicle, Router}
