import {Style, Template, Ossicle} from '../Ossicle.js'
import './MainTitle.js'

class MainApplication extends Ossicle {

  static template() { return Template.html`
    <div>
      <main-title></main-title>
      <h3>
        Hello from the "MainApplication"
      </h3>
    </div>
  `}

  static sheets() { return [
    Style.css`
      h3 { color: blue; }
    `,
    Style.sheets.bulma
  ]}

  constructor() {
    super()
    this.setup()
  }
}

customElements.define('main-application', MainApplication)
